
# ToDo: The attribute 'vpc_id' has resolved value of 'data' state. Please create a variable with resolved value and use { params.get('variable_name') } instead of resolved value of 'data' state.
aws_security_group.cluster:
  aws.ec2.security_group.present:
  - resource_id: {{ params.get("aws_security_group.cluster")}}
  - name: {{ params.get("clusterName") }}-temp-xyz-cluster
  - vpc_id: ${aws.ec2.vpc:aws_vpc.cluster:resource_id}
  - tags: {{ params.get("local_tags") + [{"Key": "Name", "Value": params.get("clusterName")+"-temp-xyz"}]}}
  - description: Cluster communication with worker nodes
