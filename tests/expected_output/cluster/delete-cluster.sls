aws_cloudwatch_log_group.redlock_flow_log_group:
  aws.cloudwatch.log_group.absent:
  - name: xyz-idem-test_redlock_flow_log_group
aws_db_subnet_group.db-subnet-group:
  aws.rds.db_subnet_group.absent:
  - name: db-subnet-group-idem-test
aws_eip.nat_eip-0:
  aws.ec2.elastic_ip.absent:
  - name: 15.236.223.139
aws_eip.nat_eip-1:
  aws.ec2.elastic_ip.absent:
  - name: 13.37.173.220
aws_eip.nat_eip-2:
  aws.ec2.elastic_ip.absent:
  - name: 13.38.205.2
aws_eks_cluster.cluster:
  aws.eks.cluster.absent:
  - name: idem-test
aws_elasticache_subnet_group.default:
  aws.elasticache.cache_subnet_group.absent:
  - name: elasticache-subnet-group-idem-test
aws_flow_log.redlock_flow_log:
  aws.ec2.flow_log.absent:
  - name: aws_flow_log.redlock_flow_log
aws_iam_policy.xyz-jenkins:
  aws.iam.policy.absent:
  - name: xyz-idem-test-jenkins
aws_iam_role.cluster:
  aws.iam.role.absent:
  - name: idem-test-temp-xyz-cluster
aws_iam_role.redlock_flow_role:
  aws.iam.role.absent:
  - name: xyz-idem-test_redlock_flow_role
aws_iam_role.xyz-jenkins:
  aws.iam.role.absent:
  - name: xyz-idem-test-jenkins
aws_iam_role_policy.redlock_flow_policy:
  aws.iam.role_policy.absent:
  - role_name: xyz-idem-test_redlock_flow_role
  - name: xyz-idem-test_redlock_flow_policy
aws_iam_role_policy_attachment.cluster-AmazonxyzClusterPolicy:
  aws.iam.role_policy_attachment.absent:
  - role_name: idem-test-temp-xyz-cluster
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzClusterPolicy
aws_iam_role_policy_attachment.cluster-AmazonxyzServicePolicy:
  aws.iam.role_policy_attachment.absent:
  - role_name: idem-test-temp-xyz-cluster
  - policy_arn: arn:aws:iam::aws:policy/AmazonxyzServicePolicy
aws_iam_role_policy_attachment.xyz-jenkins:
  aws.iam.role_policy_attachment.absent:
  - role_name: xyz-idem-test-jenkins
  - policy_arn: arn:aws:iam::123456789012:policy/xyz-idem-test-jenkins
aws_iam_user.extension-jenkins:
  aws.iam.user.absent:
  - name: extension-jenkins-idem-test
  - user_name: extension-jenkins-idem-test
aws_iam_user_policy.extension-jenkins:
  aws.iam.user_policy.absent:
  - user_name: extension-jenkins-idem-test
  - name: extension-jenkins-idem-test
aws_iam_user_policy.extension-jenkins-rolling-upgrade:
  aws.iam.user_policy.absent:
  - user_name: extension-jenkins-idem-test
  - name: extension-jenkins-rolling-upgrade-idem-test
aws_internet_gateway.cluster:
  aws.ec2.internet_gateway.absent:
  - name: igw-0eee9bba485b312a8
aws_nat_gateway.nat_gateway-0:
  aws.ec2.nat_gateway.absent:
  - name: nat-0a49a65a4bb87370a
aws_nat_gateway.nat_gateway-1:
  aws.ec2.nat_gateway.absent:
  - name: nat-0c02a1f1d590b5534
aws_nat_gateway.nat_gateway-2:
  aws.ec2.nat_gateway.absent:
  - name: nat-076cd14a28acd21b4
aws_route53_zone_association.internal-potato-beachops-io:
  aws.route53.hosted_zone_association.absent:
  - name: Z0721725PI001CQXUGYY:vpc-0738f2a523f4735bd:eu-west-3
aws_route53_zone_association.msk_kafka_domain:
  aws.route53.hosted_zone_association.absent:
  - name: Z03616682JNY9P3D3T2IR:vpc-0738f2a523f4735bd:eu-west-3
aws_route_table.cluster-0:
  aws.ec2.route_table.absent:
  - name: rtb-0516e0e06d933d9f4
aws_route_table.cluster-1:
  aws.ec2.route_table.absent:
  - name: rtb-0ee77d1deb1a1d86a
aws_route_table.cluster-2:
  aws.ec2.route_table.absent:
  - name: rtb-0b0a3c628c59ac049
aws_route_table.xyz_public-0:
  aws.ec2.route_table.absent:
  - name: rtb-01e542a8c56c9511f
aws_route_table.xyz_public-1:
  aws.ec2.route_table.absent:
  - name: rtb-0445912793473da66
aws_route_table.xyz_public-2:
  aws.ec2.route_table.absent:
  - name: rtb-05bd8f7251c25d82c
aws_security_group.cluster:
  aws.ec2.security_group.absent:
  - name: idem-test-temp-xyz-cluster
aws_security_group.cluster-node:
  aws.ec2.security_group.absent:
  - name: idem-test-temp-xyz-cluster-node
aws_security_group.cluster-node-rule-0:
  aws.ec2.security_group_rule.absent:
  - name: sgr-06fa8980bf9bffea7
aws_security_group.cluster-node-rule-1:
  aws.ec2.security_group_rule.absent:
  - name: sgr-0746837a711b2f632
aws_security_group.cluster-node-rule-2:
  aws.ec2.security_group_rule.absent:
  - name: sgr-0a410f396195a1a29
aws_security_group.cluster-node-rule-3:
  aws.ec2.security_group_rule.absent:
  - name: sgr-0b6d618ed84f17b83
aws_security_group.cluster-node-rule-4:
  aws.ec2.security_group_rule.absent:
  - name: sgr-0d08ef367d62e82f3
aws_security_group.cluster-node-rule-5:
  aws.ec2.security_group_rule.absent:
  - name: sgr-0ffb22f7ca7a0f16c
aws_security_group.cluster-rule-0:
  aws.ec2.security_group_rule.absent:
  - name: sgr-077c2651cc2eb9b1f
aws_security_group.cluster-rule-1:
  aws.ec2.security_group_rule.absent:
  - name: sgr-0a4ef3a78cdce5042
aws_security_group.nessus_vuln_scanner:
  aws.ec2.security_group.absent:
  - name: temp-20220505111435890800000003
aws_security_group.nessus_vuln_scanner-rule-0:
  aws.ec2.security_group_rule.absent:
  - name: sgr-0895dc3282b1068e5
aws_subnet.cluster-0:
  aws.ec2.subnet.absent:
  - name: subnet-050732fa4616470d9
aws_subnet.cluster-1:
  aws.ec2.subnet.absent:
  - name: subnet-05dfaa0d01a337199
aws_subnet.cluster-2:
  aws.ec2.subnet.absent:
  - name: subnet-039e53122e038d38c
aws_subnet.xyz_public_subnet-0:
  aws.ec2.subnet.absent:
  - name: subnet-09cecc8c853637d3b
aws_subnet.xyz_public_subnet-1:
  aws.ec2.subnet.absent:
  - name: subnet-0094b72dfb7ce6131
aws_subnet.xyz_public_subnet-2:
  aws.ec2.subnet.absent:
  - name: subnet-0d68d61b1ab708d42
aws_vpc.cluster:
  aws.ec2.vpc.absent:
  - name: vpc-0738f2a523f4735bd
aws_vpc_dhcp_options.vpc_options:
  aws.ec2.dhcp_option.absent:
  - name: dopt-052512c363a0a800b
