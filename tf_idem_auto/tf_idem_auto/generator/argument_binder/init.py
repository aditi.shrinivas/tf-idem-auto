from typing import Any
from typing import Dict


def arg_bind(
    hub,
    sls_data: Dict[str, Any],
) -> Dict[str, Any]:
    """
    Process the multi stage routine to determine the current requisite sequence
    """
    for arg_bind_plugin in sorted(
        hub.tf_idem_auto.generator.argument_binder._loaded.keys()
    ):
        if arg_bind_plugin == "init":
            continue
        sls_data = hub.tf_idem_auto.generator.argument_binder[arg_bind_plugin].arg_bind(
            sls_data
        )
    return sls_data
