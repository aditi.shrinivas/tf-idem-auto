# REFER init.py
#
# # Get names of all modules defined in main.tf
# list_of_modules = get_module_list(hub)
#
# if not list_of_modules:
#     hub.log.warning("Either main.tf doesn't exist or no modules found in main.tf")
#
# # Get tfvars_data, if tfvars file is present
# tfvars_data = collect_tfvars_data(hub)
#
# # Read terraform state file from S3 or 'tf_state_file_path'. Filter managed and data resources that belong
# # to modules obtained above and store processed tf_state data in file.
# # This step also gives the list of relevant resource types to describe in next step
# resource_types = hub.tf_idem_auto.tf_state_data_processor.process_tf_state_data(
#     list_of_modules
# )
# hub.log.info("Filtered Terraform state data is written in 'processed_tfstate.json'")
# hub.log.info("Idem Resource types : %s", resource_types)
#
# # Perform idem describe on list of resource_types obtained above and write the response in file
# if hub.OPT.tf_idem_auto.idem_describe:
#     hub.tf_idem_auto.idem_describe.run_idem_describe(resource_types)
#     hub.log.info(
#         "Consolidated SLS file is generated in '%s'",
#         hub.OPT.tf_idem_auto.idem_describe_path,
#     )
#
# # Read the consolidated SLS file from idem_describe_path. Filter the SLS data with
# # resources that are managed by Terraform only in all modules of cluster.
# (
#     tf_reseource_type_name_and_resource_map,
#     filtered_sls_data,
# ) = hub.tf_idem_auto.sls_file_filter.filter_sls()


def stage(hub, name: str) -> str:
    return name
